package nowowiejski.m.usertask.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import nowowiejski.m.usertask.core.network.NetworkStateProvider
import nowowiejski.m.usertask.core.network.NetworkStateProviderImpl
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
class AppModule {

    @Singleton
    @Provides
    fun provideNetworkStateProvider(
        @ApplicationContext context: Context
    ): NetworkStateProvider {
        return NetworkStateProviderImpl(context)
    }

}