package nowowiejski.m.usertask.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import nowowiejski.m.usertask.data.remote.RestApiHolder
import nowowiejski.m.usertask.data.remote.api.DailyMotionApi
import nowowiejski.m.usertask.data.remote.api.GitHubApi
import retrofit2.Retrofit
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
class NetworkingModule {

    @Provides
    @Singleton
    internal fun provideGitHubApi(
            @ApplicationContext context: Context
    ): GitHubApi = RestApiHolder.gitHubApiService(context).create(GitHubApi::class.java)

    @Provides
    @Singleton
    internal fun provideDailyMotionApi(
            @ApplicationContext context: Context
    ): DailyMotionApi = RestApiHolder.gitDailyMotionApiService(context).create(DailyMotionApi::class.java)
}