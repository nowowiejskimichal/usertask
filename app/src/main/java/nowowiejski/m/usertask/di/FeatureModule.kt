package nowowiejski.m.usertask.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import nowowiejski.m.usertask.data.datasource.dailymotion.DailyMotionDataSource
import nowowiejski.m.usertask.data.datasource.github.GitHubDataSource
import nowowiejski.m.usertask.data.repository.UserRepositoryImpl
import nowowiejski.m.usertask.domain.base.UseCaseScheduler
import nowowiejski.m.usertask.domain.repository.UserRepository
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class FeatureModule {

    @Provides
    @Singleton
    internal fun provideUseCaseScheduler() =
            UseCaseScheduler(Schedulers.io(), AndroidSchedulers.mainThread())

    @Singleton
    @Provides
    fun provideUserRepository(
            dailyMotionDataSource: DailyMotionDataSource,
            gitHubDataSource: GitHubDataSource
    ): UserRepository {
        return UserRepositoryImpl(dailyMotionDataSource, gitHubDataSource)
    }

}