package nowowiejski.m.usertask.domain

import io.reactivex.rxjava3.core.Single
import nowowiejski.m.usertask.domain.base.SingleUseCase
import nowowiejski.m.usertask.domain.base.UseCaseScheduler
import nowowiejski.m.usertask.domain.model.User
import nowowiejski.m.usertask.domain.repository.UserRepository
import javax.inject.Inject

class GetUsersUseCase @Inject constructor(
    private val userRepository: UserRepository,
    scheduler: UseCaseScheduler? = null
) : SingleUseCase<List<User>, Unit>(scheduler) {

    override fun action(params: Unit): Single<List<User>> = userRepository.getUsers()

}