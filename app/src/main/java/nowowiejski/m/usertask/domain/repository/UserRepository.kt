package nowowiejski.m.usertask.domain.repository

import io.reactivex.rxjava3.core.Single
import nowowiejski.m.usertask.domain.model.User

interface UserRepository {
    fun getUsers(): Single<List<User>>
}