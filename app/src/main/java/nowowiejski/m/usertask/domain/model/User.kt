package nowowiejski.m.usertask.domain.model

data class User(
        val userName: String,
        val avatar: String,
        val dataSource: String
)