package nowowiejski.m.usertask.presentation.ui.users.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import nowowiejski.m.usertask.databinding.ItemUserBinding
import nowowiejski.m.usertask.presentation.model.UserDisplayable
import javax.inject.Inject

class UserAdapter@Inject constructor(): ListAdapter<UserDisplayable, UserViewHolder>(UserDiff) {

    internal var clickListener: (UserDisplayable) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            ItemUserBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }
}


object UserDiff : DiffUtil.ItemCallback<UserDisplayable>() {
    override fun areItemsTheSame(oldItem: UserDisplayable, newItem: UserDisplayable): Boolean {
        return oldItem.userName == newItem.userName
    }

    override fun areContentsTheSame(oldItem: UserDisplayable, newItem: UserDisplayable) = oldItem == newItem
}