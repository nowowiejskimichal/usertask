package nowowiejski.m.usertask.presentation.ui.users

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import dagger.hilt.android.AndroidEntryPoint
import nowowiejski.m.usertask.R
import nowowiejski.m.usertask.databinding.FragmentUsersBinding
import nowowiejski.m.usertask.presentation.base.BaseFragment
import nowowiejski.m.usertask.presentation.ui.users.adapter.UserAdapter
import javax.inject.Inject

@AndroidEntryPoint
class UsersFragment : BaseFragment<FragmentUsersBinding>(R.layout.fragment_users) {

    private val viewModel: UsersViewModel by viewModels()

    @Inject
    lateinit var adapter: UserAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentUsersBinding.bind(view).apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = this@UsersFragment.viewModel
            initViews(this)
        }
        initObservers()
    }

    override fun initViews(binding: FragmentUsersBinding) {
        binding.usersView.apply {
            adapter = this@UsersFragment.adapter
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
        }

        adapter.clickListener = { user ->
            findNavController().navigate(
                UsersFragmentDirections.actionNavigateFromUsersToDetails(
                    user
                )
            )
        }
    }

    override fun initObservers() {
        viewModel.users.observe(viewLifecycleOwner) { users ->
            adapter.submitList(users)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        viewModel.saveCurrentUsers()
        super.onSaveInstanceState(outState)
    }
}