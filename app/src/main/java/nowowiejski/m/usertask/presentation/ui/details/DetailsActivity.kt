package nowowiejski.m.usertask.presentation.ui.details

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.navArgs
import nowowiejski.m.usertask.databinding.ActivityDetailsBinding

class DetailsActivity : AppCompatActivity() {

    private val args: DetailsActivityArgs by navArgs()
    private lateinit var binding: ActivityDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater, null, false).apply {
            user = args.user
        }

        setContentView(binding.root)
    }
}