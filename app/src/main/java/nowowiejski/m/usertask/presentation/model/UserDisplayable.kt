package nowowiejski.m.usertask.presentation.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import nowowiejski.m.usertask.domain.model.User

@Parcelize
data class UserDisplayable(
    val userName: String,
    val avatar: String,
    val dataSource: String
): Parcelable {

    constructor(user: User) : this(
        user.userName,
        user.avatar,
        user.dataSource
    )
}