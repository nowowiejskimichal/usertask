package nowowiejski.m.usertask.presentation.ui.users.adapter

import androidx.recyclerview.widget.RecyclerView
import nowowiejski.m.usertask.databinding.ItemUserBinding
import nowowiejski.m.usertask.presentation.model.UserDisplayable

class UserViewHolder(private val binding: ItemUserBinding)
    : RecyclerView.ViewHolder(binding.root) {

        fun bind(user: UserDisplayable, clickListener: (UserDisplayable) -> Unit){
            binding.user = user
            binding.executePendingBindings()
            itemView.setOnClickListener { clickListener(user) }
        }
}