package nowowiejski.m.usertask.presentation.ui.users

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.map
import dagger.hilt.android.lifecycle.HiltViewModel
import nowowiejski.m.usertask.domain.GetUsersUseCase
import nowowiejski.m.usertask.presentation.base.BaseViewModel
import nowowiejski.m.usertask.presentation.model.UserDisplayable
import javax.inject.Inject

@HiltViewModel
class UsersViewModel @Inject constructor(
    private val getUsersUseCase: GetUsersUseCase,
    private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {


    private val usersData: MutableLiveData<List<UserDisplayable>> =
        savedStateHandle.getLiveData<List<UserDisplayable>>(USER_KEY)
    val users: LiveData<List<UserDisplayable>> = usersData
    val isLoading = MutableLiveData(false)

    init {
        if (usersData.value == null) {
            loadData()
        }
    }

    private fun loadData() {
        isLoading.postValue(true)
        getUsersUseCase(Unit).map { users ->
            users.map { user ->
                UserDisplayable(user)
            }
        }.subscribe({ users ->
            usersData.postValue(users)
            isLoading.postValue(false)
        }, { error ->
            isLoading.postValue(false)
            Log.e(UsersViewModel::class.java.simpleName, "loadData", error)
        })
    }


    fun saveCurrentUsers() {
        savedStateHandle.set(USER_KEY, usersData.value)
    }

    companion object {
        private val USER_KEY = "users"
    }


}