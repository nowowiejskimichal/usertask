package nowowiejski.m.usertask.data.datasource.dailymotion.model

import com.google.gson.annotations.SerializedName
import nowowiejski.m.usertask.data.datasource.dailymotion.DailyMotionDataSource
import nowowiejski.m.usertask.data.remote.model.UserRemote

data class UserMotionRemote(
        @SerializedName("avatar_360_url") val avatar360Url: String,
        @SerializedName("username") val username: String
) {
    fun mapToUserRemote() = UserRemote(
            username,
            avatar360Url,
            DailyMotionDataSource.DATA_SOURCE
    )
}