package nowowiejski.m.usertask.data.remote.model

import nowowiejski.m.usertask.domain.model.User

data class UserRemote(
        val userName: String,
        val avatarUrl: String,
        val dataSource: String
) {
    fun mapToUser(): User = User(
            userName,
            avatarUrl,
            dataSource
    )
}