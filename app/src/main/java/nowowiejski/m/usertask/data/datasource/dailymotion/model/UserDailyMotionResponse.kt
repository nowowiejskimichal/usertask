package nowowiejski.m.usertask.data.datasource.dailymotion.model


import com.google.gson.annotations.SerializedName

data class UserDailyMotionResponse(
        @SerializedName("explicit") val explicit: Boolean,
        @SerializedName("has_more") val hasMore: Boolean,
        @SerializedName("limit") val limit: Int,
        @SerializedName("list") val list: List<UserMotionRemote>,
        @SerializedName("page") val page: Int,
        @SerializedName("total") val total: Int
)