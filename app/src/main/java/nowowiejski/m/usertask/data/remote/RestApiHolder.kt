package nowowiejski.m.usertask.data.remote

import android.content.Context
import nowowiejski.m.usertask.BuildConfig
import nowowiejski.m.usertask.data.remote.api.DailyMotionApi
import nowowiejski.m.usertask.data.remote.api.GitHubApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RestApiHolder {

    private val interceptor = HttpLoggingInterceptor()
        .apply {
            level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                else HttpLoggingInterceptor.Level.NONE
        }

    private fun createOkHttpClient(context: Context): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()

    fun gitHubApiService(context: Context) = Retrofit.Builder()
        .baseUrl(GitHubApi.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
        .client(createOkHttpClient(context))
        .build()

    fun gitDailyMotionApiService(context: Context) = Retrofit.Builder()
            .baseUrl(DailyMotionApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(createOkHttpClient(context))
            .build()
}