package nowowiejski.m.usertask.data.datasource.github.model


import com.google.gson.annotations.SerializedName
import nowowiejski.m.usertask.data.datasource.github.GitHubDataSource
import nowowiejski.m.usertask.data.remote.model.UserRemote

data class UserGitHubRemote(
        @SerializedName("avatar_url") val avatarUrl: String,
        @SerializedName("login") val login: String,
) {
    fun mapToUserRemote() = UserRemote(
            login,
            avatarUrl,
            GitHubDataSource.DATA_SOURCE
    )
}