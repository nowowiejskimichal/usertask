package nowowiejski.m.usertask.data.remote.api

import io.reactivex.rxjava3.core.Single
import nowowiejski.m.usertask.data.datasource.dailymotion.model.UserDailyMotionResponse
import retrofit2.http.GET

interface DailyMotionApi {

    @GET("users?fields=avatar_360_url,username")
    fun getUsers(): Single<UserDailyMotionResponse>

    companion object {
        const val BASE_URL = "https://api.dailymotion.com/"
    }
}