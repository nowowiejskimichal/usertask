package nowowiejski.m.usertask.data.repository

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.functions.BiFunction
import nowowiejski.m.usertask.data.datasource.dailymotion.DailyMotionDataSource
import nowowiejski.m.usertask.data.datasource.github.GitHubDataSource
import nowowiejski.m.usertask.data.remote.model.UserRemote
import nowowiejski.m.usertask.domain.model.User
import nowowiejski.m.usertask.domain.repository.UserRepository
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val dailyMotionSource: DailyMotionDataSource,
    private val gitHubSource: GitHubDataSource
) : UserRepository {

    override fun getUsers(): Single<List<User>> {
        gitHubSource.getUsers().toObservable()
        return gitHubSource.getUsers().zipWith(
            dailyMotionSource.getUsers(),
            { gitHubUsers, dailyMotionUsers ->
                val users = mutableListOf<UserRemote>().apply {
                    addAll(gitHubUsers)
                    addAll(dailyMotionUsers)
                }
                users.map { userRemote ->
                    userRemote.mapToUser()
                }
            })
    }

}