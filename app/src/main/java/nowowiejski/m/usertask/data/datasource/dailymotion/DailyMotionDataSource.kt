package nowowiejski.m.usertask.data.datasource.dailymotion

import io.reactivex.rxjava3.core.Single
import nowowiejski.m.usertask.data.remote.api.DailyMotionApi
import nowowiejski.m.usertask.data.remote.model.UserRemote
import javax.inject.Inject

class DailyMotionDataSource@Inject constructor(private val dailyMotionApi: DailyMotionApi) {

    fun getUsers(): Single<List<UserRemote>> = dailyMotionApi.getUsers().map { response ->
        response.list.map { user ->
            user.mapToUserRemote()
        }
    }

    companion object {
        const val DATA_SOURCE = "Daily Motion"
    }
}