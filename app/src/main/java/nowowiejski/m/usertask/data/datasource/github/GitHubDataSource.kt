package nowowiejski.m.usertask.data.datasource.github

import io.reactivex.rxjava3.core.Single
import nowowiejski.m.usertask.data.remote.api.GitHubApi
import nowowiejski.m.usertask.data.remote.model.UserRemote
import javax.inject.Inject

class GitHubDataSource@Inject constructor(private val gitHubApi: GitHubApi) {

    fun getUsers(): Single<List<UserRemote>> = gitHubApi.getUsers().map { response ->
        response.map { user ->
            user.mapToUserRemote()
        }
    }

    companion object {
        const val DATA_SOURCE = "GitHub"
    }
}