package nowowiejski.m.usertask.data.remote.api

import io.reactivex.rxjava3.core.Single
import nowowiejski.m.usertask.data.datasource.github.model.UserGitHubRemote
import retrofit2.http.GET

interface GitHubApi {

    @GET("users")
    fun getUsers(): Single<List<UserGitHubRemote>>

    companion object {
        const val BASE_URL = "https://api.github.com/"
    }
}