package nowowiejski.m.usertask.core.bindings

import android.view.View
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter(value = ["app:imageUrl", "app:placeholder"], requireAll = false)
fun setImageUrl(imageView: ImageView, imageUrl: String, @DrawableRes placeholder: Int) {
    Glide.with(imageView.context)
        .load(imageUrl)
        .centerCrop()
        .placeholder(placeholder)
        .into(imageView)
}

@BindingAdapter("goneUnless")
fun goneUnless(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}