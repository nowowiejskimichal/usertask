package nowowiejski.m.usertask.domain

import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Single
import nowowiejski.m.usertask.domain.model.User
import nowowiejski.m.usertask.domain.repository.UserRepository
import org.junit.Test

class GetUsersUseCaseTest {

    @Test
    fun `when use case is invoked then execute getUsers method from repository`() {
        val repository = mockk<UserRepository>(relaxed = true)
        val useCase = GetUsersUseCase(repository)
        val userList = mockk<List<User>>()

        every { repository.getUsers() } returns Single.just(userList)

        useCase(Unit).test()
            .assertValueCount(1)
            .assertResult(userList)
    }
}