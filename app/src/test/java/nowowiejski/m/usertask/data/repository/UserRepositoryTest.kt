package nowowiejski.m.usertask.data.repository

import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Single
import nowowiejski.m.usertask.data.datasource.dailymotion.DailyMotionDataSource
import nowowiejski.m.usertask.data.datasource.github.GitHubDataSource
import nowowiejski.m.usertask.data.remote.model.UserRemote
import nowowiejski.m.usertask.domain.model.User
import nowowiejski.m.usertask.domain.repository.UserRepository
import org.junit.Test

class UserRepositoryTest {

    @Test
    fun `fetch users from data sources`() {
        val dailyMotionDataSource = mockk<DailyMotionDataSource>()
        val gitHubDataSource = mockk<GitHubDataSource>()
        val repository: UserRepository =
                UserRepositoryImpl(dailyMotionDataSource, gitHubDataSource)

        val usersDailyMotion = listOf(UserRemote(
                "testUser",
                "wwww.google.pl",
                DailyMotionDataSource.DATA_SOURCE)
        )
        val usersGitHub = listOf(UserRemote(
                "testUser",
                "wwww.google.pl",
                GitHubDataSource.DATA_SOURCE)
        )
        val userList = listOf(
                User("testUser",
                        "wwww.google.pl",
                        GitHubDataSource.DATA_SOURCE),
                User("testUser",
                        "wwww.google.pl",
                        DailyMotionDataSource.DATA_SOURCE)
        )

        every { dailyMotionDataSource.getUsers() } returns Single.just(usersDailyMotion)
        every { gitHubDataSource.getUsers() } returns Single.just(usersGitHub)

        repository.getUsers().test()
                .assertValueCount(1)
                .assertResult(userList)
    }
}