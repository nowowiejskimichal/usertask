package nowowiejski.m.usertask.data.datasource

import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Single
import nowowiejski.m.usertask.data.datasource.github.GitHubDataSource
import nowowiejski.m.usertask.data.datasource.github.model.UserGitHubRemote
import nowowiejski.m.usertask.data.remote.api.GitHubApi
import org.junit.Test

class GitHubDataSourceTest {

    private val testResponse by lazy {
        listOf(UserGitHubRemote(
                login = "testUser",
                avatarUrl = "www.google.pl"
        ))
    }

    @Test
    fun `fetch users from GitHub source`() {
        val api = mockk<GitHubApi>()
        val dataSource = GitHubDataSource(api)

        every { api.getUsers() } returns Single.just(testResponse)

        val testObserver = dataSource.getUsers().test()
        testObserver.assertComplete()
    }
}

