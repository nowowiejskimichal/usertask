package nowowiejski.m.usertask.data.datasource

import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Single
import nowowiejski.m.usertask.data.datasource.dailymotion.DailyMotionDataSource
import nowowiejski.m.usertask.data.datasource.dailymotion.model.UserDailyMotionResponse
import nowowiejski.m.usertask.data.remote.api.DailyMotionApi
import org.junit.Test

class DailyMotionDataSourceTest {

    private val testResponse by lazy {
        UserDailyMotionResponse(
                explicit = false,
                hasMore = false,
                limit = 1,
                list = listOf(),
                page = 1,
                total = 1
        )
    }

    @Test
    fun `fetch users from Daily Motion source`() {
        val api = mockk<DailyMotionApi>()
        val dataSource = DailyMotionDataSource(api)

        every { api.getUsers() } returns Single.just(testResponse)

        val testObserver = dataSource.getUsers().test()
        testObserver.assertComplete()

    }
}